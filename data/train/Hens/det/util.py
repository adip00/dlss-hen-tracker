with open('det_raw.txt', 'r') as f_in, open('det_normalized.txt', 'w') as f_out:
    for line in f_in:
        line = line.strip().split(' ')
        modified_line = line[:1] + ['-1'] + line[1:] + ['-1', '-1', '-1', '-1']
        formatted_line = ', '.join(modified_line)
        f_out.write(formatted_line + '\n')
