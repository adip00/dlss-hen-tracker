import random

width = 1920
height = 1080

with open('det_normalized.txt', 'r') as f, open('det.txt', 'w') as o:
    for line in f:
        line = line.strip().split(',')

        x_pad = round(float(line[4]) * width / 2)
        y_pad = round(float(line[5]) * height / 2)

        line[2] = str(round(float(line[2]) * width - x_pad, 2))
        line[3] = str(round(float(line[3]) * height - y_pad, 2))
        line[4] = str(round(float(line[4]) * width, 2))
        line[5] = str(round(float(line[5]) * height, 2))
        line[6] = str(round(random.uniform(0.6, 0.99), 2))

        formatted_line = ', '.join(line)
        o.write(formatted_line + '\n')
