import os
import shutil
import re

# Source directory containing the subfolders (i.e the extracted stuff from the zip)
source_directory = 'detect'

# Destination directory for images
image_destination_directory = './mot_benchmark/train/Hens/img1/'

# Destination file for text contents, temporary
text_destination_file = 'temp.txt'

# Create the destination directories if they don't exist
os.makedirs(image_destination_directory, exist_ok=True)

# Iterate over the subfolders in the source directory
for folder_name in os.listdir(source_directory):
    folder_path = os.path.join(source_directory, folder_name)

    match = re.search(r'\d+', folder_name)
    if match:
        numeric_part = match.group()

    # print(folder_name)
    # Check if it's a directory
    if os.path.isdir(folder_path):
        # Iterate over the files and subfolders in the subfolder
        for subfolder_name in os.listdir(folder_path):
            subfolder_path = os.path.join(folder_path, subfolder_name)

            # Check if it's a subdirectory
            if os.path.isdir(subfolder_path):
                # Iterate over the files in the subdirectory
                for file_name in os.listdir(subfolder_path):
                    file_path = os.path.join(subfolder_path, file_name)

                    # Check if it's a text file
                    if file_name.lower().endswith('.txt') and os.path.isfile(file_path):
                        # Read the contents of the text file
                        with open(file_path, 'r') as file, open(text_destination_file, 'a') as dest_file:
                            for line in file:
                                line = line.strip().split()
                                line[0] = str(numeric_part)
                                formatted_line = ' '.join(line)
                                dest_file.write(formatted_line + '\n')
                            #frame_counter += 1

            # Check if it's a JPG image
            elif (subfolder_name.lower().endswith('.jpg') and os.path.isfile(subfolder_path)):
                # Rename the image file
                new_file_name = f'{str(numeric_part).zfill(6)}.jpg'
                new_file_path = os.path.join(
                    image_destination_directory, new_file_name)
                shutil.copy2(subfolder_path, new_file_path)

# Read the contents of the text file
with open('temp.txt', 'r') as file:
    lines = file.readlines()

# Sort the lines based on the first column
sorted_lines = sorted(lines, key=lambda line: int(line.split(' ')[0].strip()))

# Write the sorted lines to a new text file, final output
with open('./data/train/Hens/det/det_raw.txt', 'w') as file:
    file.writelines(sorted_lines)

print("Image and text file copying and renaming completed.")
